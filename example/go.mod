module example

go 1.18

require gitlab.com/by-implication/simple-gcs-logger v0.0.0

replace gitlab.com/by-implication/simple-gcs-logger => ../
