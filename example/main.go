package main

import gcslog "gitlab.com/by-implication/simple-gcs-logger"

const (
	NoLabels gcslog.Domain = "no labels"
)

func main() {
	gcslog.Setup(gcslog.WARNING, false)
	// log
	gcslog.Logf(gcslog.WARNING, "warning log test")
	gcslog.Logf(gcslog.INFO, "info log test")
	gcslog.LogWith(gcslog.CRITICAL, NoLabels, nil, "critical log with test")
	gcslog.LogWith(gcslog.ALERT, gcslog.Domain("with labels"), map[string]string{"key": "val"}, "critical log with test")
}
