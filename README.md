# What this is

- Simple, minimal setup, and synchronous logger designed for [google cloud structured logging](https://cloud.google.com/logging/docs/structured-logging)

## Notes

- Low performance compared to other libraries like zap

# Log Levels

- Log levels match GCS [LogSeverity](https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry#LogSeverity)
- Default = 0, Warn = 4, Emergency = 8

# Example

```go
import 	gcslog "gitlab.com/by-implication/simple-gcs-logger"

func main() {
  gcslog.Setup(gcslog.WARNING, false)
  // log
  gcslog.Logf(gcslog.WARNING, "warning log test")
  gcslog.Logf(gcslog.INFO, "info log test")
}
```

This will output

```json
{
  "severity": "WARNING",
  "message": "warning log test",
  "timestampSeconds": 1674701594,
  "logging.googleapis.com/sourceLocation": {
    "file": "main.go",
    "line": "6",
    "function": "main.main"
  }
}
```
