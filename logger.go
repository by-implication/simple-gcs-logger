package logger

import (
	"encoding/json"
	"fmt"
	"log"
	"path/filepath"
	"runtime"
	"strconv"
	"time"
)

type gcsLogger struct {
	minLevel LogLevel
	pretty   bool
}

//https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry#LogSeverity
type LogLevel int
type Domain string

var levels = []string{
	"DEFAULT",
	"DEBUG",
	"INFO",
	"NOTICE",
	"WARNING",
	"ERROR",
	"CRITICAL",
	"ALERT",
	"EMERGENCY",
}

func (level LogLevel) value() string {
	return levels[level]
}

const (
	DEFAULT LogLevel = iota
	DEBUG
	INFO
	NOTICE
	WARNING
	ERROR
	CRITICAL
	ALERT
	EMERGENCY
)

// https://cloud.google.com/logging/docs/structured-logging
type Entry struct {
	Level   string            `json:"severity"`
	Message string            `json:"message"`
	Times   int64             `json:"timestampSeconds"`
	Labels  map[string]string `json:"logging.googleapis.com/labels,omitempty"`
	Caller  caller            `json:"logging.googleapis.com/sourceLocation,omitempty"`
	SpanID  string            `json:"logging.googleapis.com/spanId,omitempty"`
	Trace   string            `json:"logging.googleapis.com/trace,omitempty"`
}

type caller struct {
	File     string `json:"file"`
	Line     string `json:"line"`
	Function string `json:"function"`
}

var logger = gcsLogger{}

func init() {
	log.SetFlags(0)
}

// Setup allows a user to set the minLevel and prettyPrint values
func Setup(minLevel LogLevel, pretty bool) {
	logger.minLevel = minLevel
	logger.pretty = pretty
}

// NewEntry allows a user to create entries manually and returns an Entry
// level indicates the LogLevel for this entry
// format, a will be used to construct the message
// This enables user to add labels, span, and trace information
// Caller info will be included on LogEntry
func NewEntry(level LogLevel, format string, a ...any) Entry {
	entry := Entry{}
	if level < logger.minLevel {
		return entry
	}
	entry.Times = time.Now().Unix()
	entry.Level = level.value()
	entry.Message = fmt.Sprintf(format, a...)
	return entry
}

// LogEntry allows a user to write an entry into stdout
// Caller info will be included
func LogEntry(entry Entry) {
	if entry.Level == "" {
		return
	}
	if entry.Caller.File == "" {
		// for manually created entries
		entry.Caller = findCaller(2)
	}
	if logger.pretty {
		log.Printf("%s [%s]: %s: %s:%s\t%s\n",
			time.Unix(entry.Times, 0).Format(time.RFC3339), entry.Level,
			entry.Caller.Function, entry.Caller.File, entry.Caller.Line,
			entry.Message,
		)
		return
	}
	// async?
	// batch then routine and defered flush?
	data, err := json.Marshal(&entry)
	if err != nil {
		return
	}
	log.Println(string(data))
}

// Logf writes a new entry to stdout
// level indicates the LogLevel for this entry
// format, a will be used to construct the message
// Caller info will be included
func Logf(level LogLevel, format string, a ...any) {
	if level < logger.minLevel {
		return
	}
	entry := NewEntry(level, format, a...)
	entry.Caller = findCaller(2)
	LogEntry(entry)
}

func LogWith(level LogLevel, domain Domain, labels map[string]string, format string, a ...any) {
	if level < logger.minLevel {
		return
	}
	entry := NewEntry(level, format, a...)
	entry.Caller = findCaller(2)
	domainString := string(domain)
	if labels == nil {
		if domainString != "" {
			entry.Labels = map[string]string{"domain": domainString}
		}
	} else {
		if domainString != "" {
			labels["domain"] = domainString
		}
		entry.Labels = labels
	}
	LogEntry(entry)
}

func findCaller(skips int) caller {
	caller := caller{}
	pc, file, line, ok := runtime.Caller(skips)
	if !ok {
		return caller
	}
	caller.File = filepath.Base(file)
	caller.Line = strconv.Itoa(line)
	details := runtime.FuncForPC(pc)
	if details != nil {
		caller.Function = details.Name()
	}
	return caller
}
